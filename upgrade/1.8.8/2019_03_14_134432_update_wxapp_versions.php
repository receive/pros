<?php

namespace We7\V188;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1552542272
 * @version 1.8.8
 */

class UpdateWxappVersions {

	/**
	 *  执行更新
	 */
	public function up()
	{
		if (pdo_fieldexists('wxapp_versions', 'tominiprogram')) {
			$all_version = pdo_getall('wxapp_versions', array(), array('id', 'tominiprogram'));
			if (!empty($all_version)) {
				foreach ($all_version as $version) {
					$tominiprogram = iunserializer($version['tominiprogram']);
					if (empty($tominiprogram) || !is_array($tominiprogram)) {
						continue;
					}
					if (is_array(current($tominiprogram)) && is_numeric(key($tominiprogram))) {
						$data = array();
						foreach ($tominiprogram as $item) {
							if (!empty($item['appid'])) {
								$data[$item['appid']] = array(
									'appid' => $item['appid'],
									'app_name' => $item['app_name']
								);
							}
						}
						if (!empty($data)) {
							pdo_update('wxapp_versions', array('tominiprogram' => iserializer($data)), array('id' => $version['id']));
						}
					}
				}
				load()->model('cache');
				cache_clean(cache_system_key('miniapp_version'));
			}
		}
	}

	/**
	 *  回滚更新
	 */
	public function down() {


	}
}
		