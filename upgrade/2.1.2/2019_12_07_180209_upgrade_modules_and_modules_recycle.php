<?php

namespace We7\V212;

defined('IN_IA') or exit('Access Denied');
/**
* [WeEngine System] Copyright (c) 2014 W7.CC
* Time: 1575712929
* @version 2.1.2
*/

class UpgradeModulesAndModulesRecycle {

/**
 *  执行更新
 */
public function up() {
	if(!pdo_fieldexists('modules', 'cloud_record')) {
		pdo_query("ALTER TABLE " . tablename('modules') . " ADD `cloud_record` TINYINT(1) NOT NULL COMMENT '云服务是否已记录模块状态：0：否；1：是';");
	}
}

/**
 *  回滚更新
 */
public function down() {


}
}
