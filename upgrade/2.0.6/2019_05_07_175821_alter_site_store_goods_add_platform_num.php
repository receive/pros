<?php

namespace We7\V206;

defined('IN_IA') or exit('Access Denied');
/**
 * [WeEngine System] Copyright (c) 2014 W7.CC
 * Time: 1557223101
 * @version 2.0.6
 */

class AlterSiteStoreGoodsAddPlatformNum {

	/**
	 *  执行更新
	 */
	public function up() {
		if (!pdo_fieldexists('site_store_goods','platform_num')) {
			pdo_query("ALTER TABLE " . tablename('site_store_goods') . " ADD `platform_num` int(10) NOT NULL DEFAULT 0 COMMENT '可创建帐号数量(除了公众号和小程序的其他类型, 为了兼容之前代码)';");
		}
	}
	
	/**
	 *  回滚更新
	 */
	public function down() {
		

	}
}
		